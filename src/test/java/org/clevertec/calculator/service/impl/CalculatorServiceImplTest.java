package org.clevertec.calculator.service.impl;

import org.clevertec.calculator.dto.CalculatorData;
import org.clevertec.calculator.exceptions.DivisionByZeroException;
import org.clevertec.calculator.exceptions.IllegalSignException;
import org.clevertec.calculator.exceptions.ResultOverflowException;
import org.clevertec.calculator.repository.CalculatorRepository;
import org.clevertec.calculator.service.CalculatorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorServiceImplTest {

    @Autowired
    private CalculatorService calculatorService;
    @MockBean
    private CalculatorRepository repository;

    @Test
    public void calculateAddiction(){
        int num1 = 2;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "+");

        given(this.repository.addition(num1, num2)).willReturn(num1+num2);

        int result = calculatorService.calculate(calculatorData);
        assertEquals(result, num1+num2);
    }

    @Test(expected = ResultOverflowException.class)
    public void calculateAddictionWithRuntimeException(){
        int num1 = 2;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "+");

        given(this.repository.addition(num1, num2)).willThrow(RuntimeException.class);

        calculatorService.calculate(calculatorData);
    }

    @Test
    public void calculateSubtract(){
        int num1 = 7;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "-");

        given(this.repository.subtract(num1, num2)).willReturn(num1-num2);

        int result = calculatorService.calculate(calculatorData);
        assertEquals(result, num1-num2);
    }

    @Test(expected = ResultOverflowException.class)
    public void calculateSubtractWithRuntimeException(){
        int num1 = 7;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "-");

        given(this.repository.subtract(num1, num2)).willThrow(RuntimeException.class);

        calculatorService.calculate(calculatorData);
    }

    @Test
    public void calculateMultiply(){
        int num1 = 2;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "*");

        given(this.repository.multiply(num1, num2)).willReturn(num1*num2);

        int result = calculatorService.calculate(calculatorData);
        assertEquals(result, num1*num2);
    }

    @Test(expected = ResultOverflowException.class)
    public void calculateMultiplyWithRuntimeException(){
        int num1 = 2;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "*");

        given(this.repository.multiply(num1, num2)).willThrow(RuntimeException.class);

        calculatorService.calculate(calculatorData);
    }

    @Test
    public void calculateDivide(){
        int num1 = 9;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "/");

        given(this.repository.divide(num1, num2)).willReturn(num1/num2);

        int result = calculatorService.calculate(calculatorData);
        assertEquals(result, num1/num2);
    }

    @Test(expected = ResultOverflowException.class)
    public void calculateDivideWithRuntimeException(){
        int num1 = 9;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "/");

        given(this.repository.divide(num1, num2)).willThrow(RuntimeException.class);

        calculatorService.calculate(calculatorData);
    }

    @Test(expected = DivisionByZeroException.class)
    public void calculateDivideWithDivisionByZeroException(){
        int num1 = 9;
        int num2 = 0;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "/");

        given(this.repository.divide(num1, num2)).willThrow(RuntimeException.class);

        calculatorService.calculate(calculatorData);
    }

    @Test(expected = IllegalSignException.class)
    public void calculateWithIncorrectMathOperationType(){
        int num1 = 9;
        int num2 = 3;
        CalculatorData calculatorData = new CalculatorData(new int[]{num1, num2}, "gf/");

        calculatorService.calculate(calculatorData);
    }
}
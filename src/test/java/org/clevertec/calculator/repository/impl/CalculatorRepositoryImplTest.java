package org.clevertec.calculator.repository.impl;

import org.clevertec.calculator.soap.org.tempuri.CalculatorSoap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorRepositoryImplTest {

    @Autowired
    private CalculatorRepositoryImpl calculatorRepository;

    @Mock
    private CalculatorSoap calculatorSoap;


    @Before
    public void initMock() {
        calculatorRepository.setCalculatorSoap(calculatorSoap);
    }

    @Test
    public void addition() {
        int num1 = 2;
        int num2 = 3;
        given(this.calculatorSoap.add(num1, num2)).willReturn(num1 + num2);

        int result = calculatorRepository.addition(num1, num2);

        Mockito.verify(calculatorSoap).add(num1, num2);
        assertEquals(result, num1 + num2);
    }

    @Test
    public void subtract() {
        int num1 = 5;
        int num2 = 3;
        given(this.calculatorSoap.subtract(num1, num2)).willReturn(num1 - num2);

        int result = calculatorRepository.subtract(num1, num2);

        Mockito.verify(calculatorSoap).subtract(num1, num2);
        assertEquals(result, num1 - num2);
    }

    @Test
    public void multiply() {
        int num1 = 2;
        int num2 = 3;
        given(this.calculatorSoap.multiply(num1, num2)).willReturn(num1 * num2);

        int result = calculatorRepository.multiply(num1, num2);

        Mockito.verify(calculatorSoap).multiply(num1, num2);
        assertEquals(result, num1 * num2);
    }

    @Test
    public void divide() {
        int num1 = 9;
        int num2 = 3;
        given(this.calculatorSoap.divide(num1, num2)).willReturn(num1 / num2);

        int result = calculatorRepository.divide(num1, num2);

        Mockito.verify(calculatorSoap).divide(num1, num2);
        assertEquals(result, num1 / num2);
    }
}
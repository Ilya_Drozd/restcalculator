package org.clevertec.calculator.handlers;

import lombok.RequiredArgsConstructor;
import org.clevertec.calculator.constants.MessageNames;
import org.clevertec.calculator.exceptions.DivisionByZeroException;
import org.clevertec.calculator.exceptions.IllegalSignException;
import org.clevertec.calculator.exceptions.ResultOverflowException;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;

@ControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(IllegalSignException.class)
    protected ResponseEntity<Object> handleIllegalSignException(IllegalSignException e) {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.ILLEGAL_SIGN_MESSAGE,
                new Object[]{e.getMessage()}, Locale.UK) ,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResultOverflowException.class)
    protected ResponseEntity<Object> handleResultOverflowException() {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.RESULT_OVERFLOW_MESSAGE,
                null, Locale.UK) ,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DivisionByZeroException.class)
    protected ResponseEntity<Object> handleDivisionByZeroException() {
        return new ResponseEntity<>(messageSource.getMessage(MessageNames.DIVISION_BY_ZERO_MESSAGE,
                null, Locale.UK) ,HttpStatus.BAD_REQUEST);
    }
}


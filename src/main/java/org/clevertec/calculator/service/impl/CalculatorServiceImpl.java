package org.clevertec.calculator.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.clevertec.calculator.constants.MathOperations;
import org.clevertec.calculator.constants.MessageNames;
import org.clevertec.calculator.dto.CalculatorData;
import org.clevertec.calculator.exceptions.DivisionByZeroException;
import org.clevertec.calculator.exceptions.IllegalSignException;
import org.clevertec.calculator.exceptions.ResultOverflowException;
import org.clevertec.calculator.repository.CalculatorRepository;
import org.clevertec.calculator.service.CalculatorService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class CalculatorServiceImpl implements CalculatorService {

    private final CalculatorRepository repository;
    private final MessageSource messageSource;

    @Override
    public int calculate(CalculatorData calculatorData) {
        int num1 = calculatorData.getNumbers()[0];
        int num2 = calculatorData.getNumbers()[1];
        String operationType = calculatorData.getOperationType();

        switch (operationType) {
            case MathOperations.ADDICTION:
                return addition(num1, num2);
            case MathOperations.SUBTRACTION:
                return subtract(num1, num2);
            case MathOperations.MULTIPLICATION:
                return multiply(num1, num2);
            case MathOperations.DIVISION:
                return divide(num1, num2);
            default:
                log.error(messageSource.getMessage(MessageNames.ILLEGAL_SIGN_MESSAGE,
                        new Object[]{operationType}, Locale.UK));
                throw new IllegalSignException(operationType);
        }
    }

    private int addition(int num1, int num2) {
        int result;
        try {
            result = repository.addition(num1, num2);
            log.info(messageSource.getMessage(MessageNames.ADDICTION_MESSAGE,
                    new Object[]{num1, num2, result}, Locale.UK));
        } catch (RuntimeException e) {
            log.error(messageSource.getMessage(MessageNames.RESULT_OVERFLOW_MESSAGE,
                    null, Locale.UK));
            throw new ResultOverflowException();
        }
        return result;
    }

    private int subtract(int num1, int num2) {
        int result;
        try {
            result = repository.subtract(num1, num2);
            log.info(messageSource.getMessage(MessageNames.SUBTRACT_MESSAGE,
                    new Object[]{num1, num2, result}, Locale.UK));
        } catch (RuntimeException e) {
            log.error(messageSource.getMessage(MessageNames.RESULT_OVERFLOW_MESSAGE,
                    null, Locale.UK));
            throw new ResultOverflowException();
        }
        return result;
    }

    private int multiply(int num1, int num2) {
        int result;
        try {
            result = repository.multiply(num1, num2);
            log.info(messageSource.getMessage(MessageNames.MULTIPLY_MESSAGE,
                    new Object[]{num1, num2, result}, Locale.UK));
        } catch (RuntimeException e) {
            log.error(messageSource.getMessage(MessageNames.RESULT_OVERFLOW_MESSAGE,
                    null, Locale.UK));
            throw new ResultOverflowException();
        }
        return result;
    }

    private int divide(int num1, int num2) {
        if(num2 !=0){
            int result;
            try {
                result = repository.divide(num1, num2);
                log.info(messageSource.getMessage(MessageNames.DIVIDE_MESSAGE,
                        new Object[]{num1, num2, result}, Locale.UK));
            } catch (RuntimeException e) {
                log.error(messageSource.getMessage(MessageNames.RESULT_OVERFLOW_MESSAGE,
                        null, Locale.UK));
                throw new ResultOverflowException();
            }
            return result;
        } else {
            log.error(messageSource.getMessage(MessageNames.DIVISION_BY_ZERO_MESSAGE,
                    null, Locale.UK));
            throw new DivisionByZeroException();
        }
    }

}

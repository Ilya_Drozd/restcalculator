package org.clevertec.calculator.service;

import org.clevertec.calculator.dto.CalculatorData;

public interface CalculatorService {
    int calculate(CalculatorData calculatorData);
}

package org.clevertec.calculator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculatorData {

    private int[] numbers = new int[2];

    private String operationType;

}

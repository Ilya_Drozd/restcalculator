package org.clevertec.calculator.constants;

public class MathOperations {
    public static final String ADDICTION = "+";
    public static final String SUBTRACTION = "-";
    public static final String MULTIPLICATION = "*";
    public static final String DIVISION = "/";
}

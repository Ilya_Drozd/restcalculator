package org.clevertec.calculator.constants;

public class MessageNames {

    public static final String ADDICTION_MESSAGE = "message.addictionMessage";
    public static final String SUBTRACT_MESSAGE = "message.subtractMessage";
    public static final String MULTIPLY_MESSAGE = "message.multiplyMessage";
    public static final String DIVIDE_MESSAGE = "message.divideMessage";
    public static final String ILLEGAL_SIGN_MESSAGE = "message.illegalSignMessage";
    public static final String RESULT_OVERFLOW_MESSAGE = "message.resultOverflowMessage";
    public static final String DIVISION_BY_ZERO_MESSAGE = "message.divisionByZeroMessage";

    public static final String UTF_8 = "UTF-8";

}

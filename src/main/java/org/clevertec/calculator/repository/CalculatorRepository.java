package org.clevertec.calculator.repository;

public interface CalculatorRepository {
    int addition(int num1, int num2);
    int subtract(int num1, int num2);
    int multiply(int num1, int num2);
    int divide(int num1, int num2);
}

package org.clevertec.calculator.repository.impl;

import lombok.Setter;
import org.clevertec.calculator.repository.CalculatorRepository;
import org.clevertec.calculator.soap.org.tempuri.Calculator;
import org.clevertec.calculator.soap.org.tempuri.CalculatorSoap;
import org.springframework.stereotype.Component;


@Component
public class CalculatorRepositoryImpl implements CalculatorRepository {

    @Setter
    private CalculatorSoap calculatorSoap = new Calculator().getCalculatorSoap();


    @Override
    public int addition(int num1, int num2) {
        return calculatorSoap.add(num1, num2);
    }

    @Override
    public int subtract(int num1, int num2) {
        return calculatorSoap.subtract(num1, num2);
    }

    @Override
    public int multiply(int num1, int num2) {
        return calculatorSoap.multiply(num1, num2);
    }

    @Override
    public int divide(int num1, int num2) {
        return calculatorSoap.divide(num1, num2);
    }
}

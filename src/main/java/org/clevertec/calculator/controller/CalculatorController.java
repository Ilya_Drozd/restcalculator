package org.clevertec.calculator.controller;

import lombok.RequiredArgsConstructor;
import org.clevertec.calculator.dto.CalculatorData;
import org.clevertec.calculator.service.CalculatorService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calculator")
@RequiredArgsConstructor
public class CalculatorController {

    private final CalculatorService service;

    @PostMapping(value = "/calculate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> calculate(@RequestBody CalculatorData data) {
        return ResponseEntity.ok(service.calculate(data));
    }

}

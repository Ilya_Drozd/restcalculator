# Calculator

## Description
REST service for working with SOAP calculator

## REST-services:
    
#### Calculate
#### POST [http://localhost:8081/calculator/calculate](http://localhost:8081/calculator/calculate)
#### calculating (addition, subtraction, multiplication and division) of two numbers

        Example of request body:
        {
            "numbers": [ 3, 4 ],
        	"operationType": "+"
        }

---